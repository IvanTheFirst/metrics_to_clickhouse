package main

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	//var cert_file_path, key_file_path, ca_file_path, hostname, port string
	//var args [5]string
	args := read_args()
	var cert_file_path string = args[0]
	var key_file_path string = args[1]
	var ca_file_path string = args[2]
	var hostname string = args[3]
	var port string = args[4]
	var readtimeout string = args[5]
	s, err := strconv.ParseInt(readtimeout, 10, 16)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	log.Println(s)
	tls_tcp_server(cert_file_path, key_file_path, ca_file_path, hostname, port, s)
	//tcp_server(hostname, port)
}

func read_args() [6]string {

	hostname_default, err := os.Hostname()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	var cert_file_path, key_file_path, ca_file_path, hostname, port, readtimeout string

	// flags declaration using flag package
	flag.StringVar(&cert_file_path, "cf", "./cert.pem", "Specify path to certificate file")
	flag.StringVar(&key_file_path, "kf", "./key.pem", "Specify path to key file")
	flag.StringVar(&ca_file_path, "ca", "./ca.pem", "Specify path to certificate authority file")
	flag.StringVar(&hostname, "h", hostname_default, "hostname to server bind")
	flag.StringVar(&port, "p", "4222", "port to server bind")
	flag.StringVar(&readtimeout, "rt", "100", "conn SetReadDeadline")

	flag.Usage = func() {
		flag.PrintDefaults() // prints default usage
	}

	flag.Parse()

	output := [6]string{cert_file_path, key_file_path, ca_file_path, hostname, port, readtimeout}

	return output
}

func tcp_server(hostname string,
	port string) {
	log.Println("Launching server...")
	listener, _ := net.Listen("tcp", hostname+":"+port)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("server: accept: %s", err)
			break
		}
		go handleClient(conn, 1000)
	}
}

func tls_tcp_server(cert_file_path string,
	key_file_path string,
	ca_file_path string,
	hostname string,
	port string,
	readtimeout int64) {
	cert, err := tls.LoadX509KeyPair(cert_file_path, key_file_path)
	if err != nil {
		log.Fatalf("server: loadkeys: %s", err)
		panic(err)
	}
	config := tls.Config{Certificates: []tls.Certificate{cert}}
	config.Rand = rand.Reader
	service := hostname + ":" + string(port)
	//это порт с tls
	//listener, err := tls.Listen("tcp", service, &config)
	// поднимаем порт без tls
	listener, _ := net.Listen("tcp", service)
	if err != nil {
		log.Fatalf("server: listen: %s", err)
		panic(err)
	}
	log.Print("server: listening")
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("server: accept: %s", err)
			break
		}
		defer conn.Close()
		log.Printf("server: accepted from %s", conn.RemoteAddr())

		//отправляем ответ INFO незашифрованный
		response := []byte(nats_proto_info_reply(conn.LocalAddr().String(), 0, conn.RemoteAddr().String()))
		conn.Write(response)

		//переводим подключение в tls
		tls_connection := tls.Server(conn, &config)
		/*tlscon, ok := tls_connection.(*tls.Conn)
		if ok {
			log.Print("ok=true")
			state := tlscon.ConnectionState()
			for _, v := range state.PeerCertificates {
				log.Print(x509.MarshalPKIXPublicKey(v.PublicKey))
			}
		}
		*/
		go handleClient(tls_connection, readtimeout)
	}
	/*
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("server: accept: %s", err)
			panic(err)
		}
		defer conn.Close()
		tlscon, ok := conn.(*tls.Conn)
		if ok {
			log.Print("ok=true")
			state := tlscon.ConnectionState()
			for _, v := range state.PeerCertificates {
				log.Print(x509.MarshalPKIXPublicKey(v.PublicKey))
			}
		}
		for {
			message, _ := bufio.NewReader(tlscon).ReadString('\n')
			fmt.Print("server: message received:", message)
		}
	*/
}

func handleClient(conn net.Conn, readtimeout int64) {
	defer conn.Close()
	var timeout = time.Duration(readtimeout)
	conn.SetReadDeadline(time.Now().Add(time.Millisecond * timeout))
	//buf := make([]byte, 1024)
	for {
		log.Print("server: conn: waiting")
		msg, err := bufio.NewReader(conn).ReadString('\n')
		//msg, err := conn.Read(buf)
		if err != nil {
			log.Printf("server: conn: read: %s", err)
			break
		}
		if strings.Contains(string(msg), "PING") {
			conn.Write([]byte("PONG\r\n"))
		}
		log.Printf("server: conn: echo %q\n", msg)
		/*n, err := conn.Read(buf)
		if err != nil {
			if err != nil {
				log.Printf("server: conn: read: %s", err)
			}
			break
		}
		log.Printf("server: conn: echo %q\n", string(buf[:n]))
		*/
	}
	log.Println("server: conn: closed")
}

func nats_proto_info_reply(ip_port string, client_id int64, client_ip_port string) []byte {
	server_name, err := os.Hostname()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	port := strings.Split(ip_port, ":")[1]
	client_ip := strings.Split(client_ip_port, ":")[0]
	template := "{\"server_id\":\"1\",\"server_name\":\"" + server_name + "\",\"version\":\"2.6.6\",\"proto\":1,\"git_commit\":\"878afad\",\"go\":\"go1.16.10\",\"host\":\"0.0.0.0\",\"port\":" + port + ",\"headers\":true,\"tls_required\":true,\"max_payload\":1048576,\"jetstream\":false,\"client_id\":" + fmt.Sprint(client_id) + ",\"client_ip\":\"" + client_ip + "\",\"cluster\":\"monitoring\",\"connect_urls\":[\"" + ip_port + "\"]}"
	info_reply := bytes.Join([][]byte{[]byte("INFO"), []byte(template), []byte("\r\n")}, []byte(" "))
	return info_reply
}
